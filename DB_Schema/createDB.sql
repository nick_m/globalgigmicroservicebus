SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `microsrvbus` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
SHOW WARNINGS;
USE `microsrvbus` ;

-- -----------------------------------------------------
-- Table `microsrvbus`.`customer_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`customer_category` (
  `customer_category_id` INT NOT NULL AUTO_INCREMENT,
  `customer_category_cd_str` VARCHAR(255) NOT NULL,
  `customer_category_nm_str` VARCHAR(255) NULL,
  PRIMARY KEY (`customer_category_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`reason`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`reason` (
  `reason_id` INT NOT NULL AUTO_INCREMENT,
  `reason_cd_str` VARCHAR(45) NOT NULL,
  `reason_nm_str` VARCHAR(45) NULL,
  PRIMARY KEY (`reason_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`source_of_signup`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`source_of_signup` (
  `source_of_signup_id` INT NOT NULL AUTO_INCREMENT,
  `source_of_signup_cd` VARCHAR(45) NOT NULL,
  `source_of_signup_nm` VARCHAR(45) NULL,
  PRIMARY KEY (`source_of_signup_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`language`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`language` (
  `language_id` INT NOT NULL AUTO_INCREMENT,
  `lang_cd_str` VARCHAR(255) NOT NULL,
  `lang_nm_str` VARCHAR(255) NULL,
  PRIMARY KEY (`language_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`location`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`location` (
  `location_id` INT NOT NULL AUTO_INCREMENT,
  `location_cd_str` VARCHAR(255) NOT NULL,
  `location_nm_str` VARCHAR(255) NULL,
  PRIMARY KEY (`location_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`privilege`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`privilege` (
  `privilege_id` INT NOT NULL AUTO_INCREMENT,
  `edit_offr_bln` BIT(1) NULL,
  `edit_prd_bln` BIT(1) NULL,
  `provsn_sim_bln` BIT(1) NULL,
  `is_admn_bln` BIT(1) NULL,
  `is_dflt_priv_bln` BIT(1) NULL,
  `nm_str` VARCHAR(255) NULL,
  `can_access_blog_entry_level3` BIT(1) NULL,
  `can_create_white_label_product` BIT(1) NULL,
  `can_modify_white_label_product` BIT(1) NULL,
  `can_modify_white_label_solution` BIT(1) NULL,
  `dsrt_bln` BIT(1) NULL,
  `is_whitlblsol_admn_bln` BIT(1) NULL,
  `is_custmr_care_bln` BIT(1) NULL,
  PRIMARY KEY (`privilege_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`cms_privilege`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`cms_privilege` (
  `cms_privilege_id` INT NOT NULL AUTO_INCREMENT,
  `can_cret_bln` BIT(1) NULL,
  `can_edit_bln` BIT(1) NULL,
  `can_publs_bln` BIT(1) NULL,
  `can_review_bln` BIT(1) NULL,
  `can_unpubls_bln` BIT(1) NULL,
  `nm_str` VARCHAR(255) NULL,
  PRIMARY KEY (`cms_privilege_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`user` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `user_cd` VARCHAR(45) NOT NULL,
  `invalid_login_count` TINYINT NULL DEFAULT 0,
  `password` VARCHAR(45) NOT NULL,
  `password1` VARCHAR(45) NULL,
  `password2` VARCHAR(45) NULL,
  `password3` VARCHAR(45) NULL,
  `password4` VARCHAR(45) NULL,
  `password5` VARCHAR(45) NULL,
  `password_change_date` DATETIME NULL,
  `password_expiry_date` DATETIME NULL,
  `status` BIT NULL DEFAULT 0,
  `user_name` VARCHAR(45) NOT NULL,
  `department_name` VARCHAR(45) NULL,
  `email_address` VARCHAR(45) NOT NULL,
  `language_id` INT NULL,
  `telephone_no` VARCHAR(45) NULL,
  `user_desc` VARCHAR(255) NULL,
  `multiple_login` BIT NULL,
  `location_id` INT NULL,
  PRIMARY KEY (`user_id`),
  INDEX `fk_user_language1_idx` (`language_id` ASC),
  INDEX `fk_user_location1_idx` (`location_id` ASC),
  CONSTRAINT `fk_user_language1`
    FOREIGN KEY (`language_id`)
    REFERENCES `microsrvbus`.`language` (`language_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_location1`
    FOREIGN KEY (`location_id`)
    REFERENCES `microsrvbus`.`location` (`location_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Users of the system (admins, operators, support stuff, etc)';

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`property`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`property` (
  `property_id` INT NOT NULL AUTO_INCREMENT,
  `entity_belonged_to` VARCHAR(45) NOT NULL,
  `property_name` VARCHAR(45) NOT NULL,
  `property_type` VARCHAR(45) NULL,
  PRIMARY KEY (`property_id`))
ENGINE = InnoDB
COMMENT = 'The additional properties dictionary  for different entities /* comment truncated */ /* (users, customers, accounts, etc). I.e. some kind of dynamic structures extension mechanism*/';

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`user_property`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`user_property` (
  `user_id` INT NOT NULL,
  `property_id` INT NOT NULL,
  `value` VARCHAR(255) NULL,
  PRIMARY KEY (`user_id`, `property_id`),
  INDEX `fk_user_has_property_property1_idx` (`property_id` ASC),
  INDEX `fk_user_has_property_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_user_has_property_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `microsrvbus`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_property_property1`
    FOREIGN KEY (`property_id`)
    REFERENCES `microsrvbus`.`property` (`property_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`country_code`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`country_code` (
  `country_code_id` INT NOT NULL AUTO_INCREMENT,
  `cd_str` VARCHAR(255) NOT NULL,
  `nm_str` VARCHAR(255) NOT NULL,
  `is_dsbld_bln` BIT(1) NULL,
  PRIMARY KEY (`country_code_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`server_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`server_group` (
  `server_group_id` INT NOT NULL,
  `srv_grp_name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`server_group_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`customer` (
  `customer_id` INT NOT NULL AUTO_INCREMENT,
  `contact_no` VARCHAR(45) NULL,
  `contact_person` VARCHAR(255) NULL DEFAULT '',
  `contract_no` VARCHAR(45) NULL,
  `salutation_prefix` VARCHAR(255) NULL DEFAULT '',
  `salutation_postfix` VARCHAR(255) NULL,
  `first_name` VARCHAR(255) NULL,
  `middle_name` VARCHAR(255) NULL,
  `last_name` VARCHAR(255) NOT NULL,
  `phone_no` VARCHAR(255) NULL,
  `fax_no` VARCHAR(255) NULL,
  `email_address` VARCHAR(255) NULL,
  `start_date` DATE NULL,
  `stop_date` DATE NULL,
  `contract_stop_date` DATE NULL,
  `customercol` VARCHAR(255) NULL,
  `customercategory_id` INT NOT NULL,
  `customercategory_cd` VARCHAR(45) NULL,
  `no_of_accounts` TINYINT NULL DEFAULT 1,
  `reason_id` INT NOT NULL,
  `reason_cd` VARCHAR(45) NULL,
  `source_of_signup_id` INT NOT NULL,
  `source_of_signup_cd` VARCHAR(45) NULL,
  `sales_agent_id` INT NULL,
  `sales_agent_cd` VARCHAR(45) NULL,
  PRIMARY KEY (`customer_id`),
  INDEX `fk_customer_customercategory1_idx` (`customercategory_id` ASC),
  INDEX `fk_customer_reason1_idx` (`reason_id` ASC),
  INDEX `fk_customer_source_of_signup1_idx` (`source_of_signup_id` ASC),
  INDEX `fk_customer_user1_idx` (`sales_agent_id` ASC),
  CONSTRAINT `fk_customer_customercategory1`
    FOREIGN KEY (`customercategory_id`)
    REFERENCES `microsrvbus`.`customer_category` (`customer_category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_reason1`
    FOREIGN KEY (`reason_id`)
    REFERENCES `microsrvbus`.`reason` (`reason_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_source_of_signup1`
    FOREIGN KEY (`source_of_signup_id`)
    REFERENCES `microsrvbus`.`source_of_signup` (`source_of_signup_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_user1`
    FOREIGN KEY (`sales_agent_id`)
    REFERENCES `microsrvbus`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`customer_property`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`customer_property` (
  `customer_id` INT NOT NULL,
  `property_id` INT NOT NULL,
  `value` VARCHAR(255) NULL,
  PRIMARY KEY (`customer_id`, `property_id`),
  INDEX `fk_customer_has_property_property1_idx` (`property_id` ASC),
  INDEX `fk_customer_has_property_customer1_idx` (`customer_id` ASC),
  CONSTRAINT `fk_customer_has_property_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `microsrvbus`.`customer` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_has_property_property1`
    FOREIGN KEY (`property_id`)
    REFERENCES `microsrvbus`.`property` (`property_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`customer_remark`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`customer_remark` (
  `customer_remark_id` INT NOT NULL AUTO_INCREMENT,
  `remark_desc` TEXT NOT NULL,
  `priority` VARCHAR(45) NULL DEFAULT 'Normal',
  `remarked_on` DATETIME NOT NULL,
  `customer_id` INT NOT NULL,
  `agent_user_id` INT NOT NULL,
  PRIMARY KEY (`customer_remark_id`),
  INDEX `fk_remark_customer1_idx` (`customer_id` ASC),
  INDEX `fk_remark_user1_idx` (`agent_user_id` ASC),
  CONSTRAINT `fk_remark_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `microsrvbus`.`customer` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_remark_user1`
    FOREIGN KEY (`agent_user_id`)
    REFERENCES `microsrvbus`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`bill_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`bill_group` (
  `bill_group_id` INT NOT NULL AUTO_INCREMENT,
  `group_descr` VARCHAR(255) NOT NULL,
  `group_nm_str` VARCHAR(255) NULL,
  `group_crtd_dt` DATETIME NULL,
  `group_mdfd_dt` DATETIME NULL,
  `group_dsbld_bln` BIT(1) NULL,
  `group_promo_grp_bln` BIT(1) NULL,
  `group_dsbld_promo_bln` BIT(1) NULL,
  `mdfdby_user_id` INT NOT NULL,
  `crtdby_user_id` INT NOT NULL,
  PRIMARY KEY (`bill_group_id`),
  INDEX `fk_bill_group_user1_idx` (`mdfdby_user_id` ASC),
  INDEX `fk_bill_group_user2_idx` (`crtdby_user_id` ASC),
  CONSTRAINT `fk_bill_group_user1`
    FOREIGN KEY (`mdfdby_user_id`)
    REFERENCES `microsrvbus`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bill_group_user2`
    FOREIGN KEY (`crtdby_user_id`)
    REFERENCES `microsrvbus`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`discount_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`discount_group` (
  `discount_group_id` INT NOT NULL,
  `dsk_grp_descr` VARCHAR(255) NULL,
  `dsk_nm_str` VARCHAR(255) NULL,
  `dsk_grp_crtd_dt` DATETIME NULL,
  `dsk_grp_mdfd_dt` DATETIME NULL,
  `mdfdby_user_id` INT NOT NULL,
  `crtdby_user_id1` INT NOT NULL,
  PRIMARY KEY (`discount_group_id`),
  INDEX `fk_discount_group_user1_idx` (`mdfdby_user_id` ASC),
  INDEX `fk_discount_group_user2_idx` (`crtdby_user_id1` ASC),
  CONSTRAINT `fk_discount_group_user1`
    FOREIGN KEY (`mdfdby_user_id`)
    REFERENCES `microsrvbus`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_discount_group_user2`
    FOREIGN KEY (`crtdby_user_id1`)
    REFERENCES `microsrvbus`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`billable_acc`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`billable_acc` (
  `billable_acc_id` INT NOT NULL AUTO_INCREMENT,
  `billable_acc_no` VARCHAR(255) NOT NULL,
  `customer_id` INT NOT NULL,
  `reporting_media` VARCHAR(255) NULL,
  `number_copies` TINYINT NOT NULL DEFAULT 1,
  `language_id` INT NOT NULL,
  `bill_group_id` INT NOT NULL,
  `discount_group_id` INT NOT NULL,
  PRIMARY KEY (`billable_acc_id`),
  INDEX `fk_billable_acc_customer1_idx` (`customer_id` ASC),
  INDEX `fk_billable_acc_language1_idx` (`language_id` ASC),
  INDEX `fk_billable_acc_bill_group1_idx` (`bill_group_id` ASC),
  INDEX `fk_billable_acc_discount_group1_idx` (`discount_group_id` ASC),
  CONSTRAINT `fk_billable_acc_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `microsrvbus`.`customer` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_billable_acc_language1`
    FOREIGN KEY (`language_id`)
    REFERENCES `microsrvbus`.`language` (`language_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_billable_acc_bill_group1`
    FOREIGN KEY (`bill_group_id`)
    REFERENCES `microsrvbus`.`bill_group` (`bill_group_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_billable_acc_discount_group1`
    FOREIGN KEY (`discount_group_id`)
    REFERENCES `microsrvbus`.`discount_group` (`discount_group_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`nonbillable_acc`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`nonbillable_acc` (
  `nonbillable_acc_id` INT NOT NULL AUTO_INCREMENT,
  `nonbillable_acc_no` VARCHAR(255) NOT NULL,
  `accountdesc` VARCHAR(255) NULL,
  `billable_acc_id` INT NOT NULL,
  `status` CHAR(1) NULL,
  PRIMARY KEY (`nonbillable_acc_id`),
  INDEX `fk_nonbillable_acc_billable_acc1_idx` (`billable_acc_id` ASC),
  CONSTRAINT `fk_nonbillable_acc_billable_acc1`
    FOREIGN KEY (`billable_acc_id`)
    REFERENCES `microsrvbus`.`billable_acc` (`billable_acc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`account_remark`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`account_remark` (
  `account_remark_id` INT NOT NULL AUTO_INCREMENT,
  `remark_desc` TEXT NOT NULL,
  `priority` VARCHAR(45) NULL DEFAULT 'Normal',
  `remarked_on` DATETIME NOT NULL,
  `billable_acc_id` INT NULL,
  `nonbillable_acc_id` INT NULL,
  `agent_user_id` INT NOT NULL,
  PRIMARY KEY (`account_remark_id`),
  INDEX `fk_account_remark_billable_acc1_idx` (`billable_acc_id` ASC),
  INDEX `fk_account_remark_nonbillable_acc1_idx` (`nonbillable_acc_id` ASC),
  INDEX `fk_account_remark_user1_idx` (`agent_user_id` ASC),
  CONSTRAINT `fk_account_remark_billable_acc1`
    FOREIGN KEY (`billable_acc_id`)
    REFERENCES `microsrvbus`.`billable_acc` (`billable_acc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_account_remark_nonbillable_acc1`
    FOREIGN KEY (`nonbillable_acc_id`)
    REFERENCES `microsrvbus`.`nonbillable_acc` (`nonbillable_acc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_account_remark_user1`
    FOREIGN KEY (`agent_user_id`)
    REFERENCES `microsrvbus`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;


-- -----------------------------------------------------
-- Table `microsrvbus`.`billable_acc_property`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`billable_acc_property` (
  `billable_acc_id` INT NOT NULL,
  `property_id` INT NOT NULL,
  `value` VARCHAR(255) NULL,
  PRIMARY KEY (`billable_acc_id`, `property_id`),
  INDEX `fk_billable_acc_has_property_property1_idx` (`property_id` ASC),
  INDEX `fk_billable_acc_has_property_billable_acc1_idx` (`billable_acc_id` ASC),
  CONSTRAINT `fk_billable_acc_has_property_billable_acc1`
    FOREIGN KEY (`billable_acc_id`)
    REFERENCES `microsrvbus`.`billable_acc` (`billable_acc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_billable_acc_has_property_property1`
    FOREIGN KEY (`property_id`)
    REFERENCES `microsrvbus`.`property` (`property_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`billable_acc_hist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`billable_acc_hist` (
  `billable_acc_hist_id` INT NOT NULL AUTO_INCREMENT,
  `billable_acc_id` INT NOT NULL,
  `billable_acc_no` VARCHAR(255) NOT NULL,
  `actual_due` DATETIME NOT NULL,
  `customer_id` INT NOT NULL,
  `reporting_media` VARCHAR(255) NULL,
  `number_copies` TINYINT NOT NULL DEFAULT 1,
  `language_id` INT NOT NULL,
  `bill_group_id` INT NOT NULL,
  `discount_group_id` INT NOT NULL,
  PRIMARY KEY (`billable_acc_hist_id`),
  INDEX `fk_billable_acc_hist_billable_acc1_idx` (`billable_acc_id` ASC),
  CONSTRAINT `fk_billable_acc_hist_billable_acc1`
    FOREIGN KEY (`billable_acc_id`)
    REFERENCES `microsrvbus`.`billable_acc` (`billable_acc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`billing_cycle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`billing_cycle` (
  `billing_cycle_id` INT NOT NULL AUTO_INCREMENT,
  `billable_acc_id` INT NOT NULL,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  PRIMARY KEY (`billing_cycle_id`, `billable_acc_id`),
  INDEX `fk_billing_cycle_billable_acc1_idx` (`billable_acc_id` ASC),
  CONSTRAINT `fk_billing_cycle_billable_acc1`
    FOREIGN KEY (`billable_acc_id`)
    REFERENCES `microsrvbus`.`billable_acc` (`billable_acc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`GSM`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`GSM` (
  `gsm_id` INT NOT NULL AUTO_INCREMENT,
  `gsm_name` VARCHAR(45) NOT NULL,
  `gsm_desc` VARCHAR(255) NULL,
  `gsm_mask` VARCHAR(45) NOT NULL,
  `country_access` VARCHAR(255) NULL,
  `country_count` VARCHAR(45) NULL,
  PRIMARY KEY (`gsm_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`server_group_country_code`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`server_group_country_code` (
  `server_group_server_group_id` INT NOT NULL,
  `country_code_country_code_id` INT NOT NULL,
  PRIMARY KEY (`server_group_server_group_id`, `country_code_country_code_id`),
  INDEX `fk_server_group_has_country_code_country_code1_idx` (`country_code_country_code_id` ASC),
  INDEX `fk_server_group_has_country_code_server_group1_idx` (`server_group_server_group_id` ASC),
  CONSTRAINT `fk_server_group_has_country_code_server_group1`
    FOREIGN KEY (`server_group_server_group_id`)
    REFERENCES `microsrvbus`.`server_group` (`server_group_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_server_group_has_country_code_country_code1`
    FOREIGN KEY (`country_code_country_code_id`)
    REFERENCES `microsrvbus`.`country_code` (`country_code_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`GSM_service`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`GSM_service` (
  `GSM_service_id` INT NOT NULL,
  `nonbillable_acc_id` INT NOT NULL,
  PRIMARY KEY (`GSM_service_id`, `nonbillable_acc_id`),
  INDEX `fk_GSM_has_nonbillable_acc_nonbillable_acc1_idx` (`nonbillable_acc_id` ASC),
  INDEX `fk_GSM_has_nonbillable_acc_GSM1_idx` (`GSM_service_id` ASC),
  CONSTRAINT `fk_GSM_has_nonbillable_acc_GSM1`
    FOREIGN KEY (`GSM_service_id`)
    REFERENCES `microsrvbus`.`GSM` (`gsm_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_GSM_has_nonbillable_acc_nonbillable_acc1`
    FOREIGN KEY (`nonbillable_acc_id`)
    REFERENCES `microsrvbus`.`nonbillable_acc` (`nonbillable_acc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`gsm_plan_offered`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`gsm_plan_offered` (
  `gsm_plan_offered_id` INT NOT NULL AUTO_INCREMENT,
  `plan_name` VARCHAR(255) NOT NULL,
  `nrc` DECIMAL(10,2) NOT NULL DEFAULT 0,
  `rc` DECIMAL(10,2) NOT NULL DEFAULT 0,
  `gsm_id` INT NOT NULL,
  PRIMARY KEY (`gsm_plan_offered_id`),
  INDEX `fk_plan_offered_GSM1_idx` (`gsm_id` ASC),
  CONSTRAINT `fk_plan_offered_GSM1`
    FOREIGN KEY (`gsm_id`)
    REFERENCES `microsrvbus`.`GSM` (`gsm_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`bundle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`bundle` (
  `bundle_id` INT NOT NULL AUTO_INCREMENT,
  `bundle_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`bundle_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`bundle_service`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`bundle_service` (
  `bundle_id` INT NOT NULL,
  `nonbillable_acc_id` INT NOT NULL,
  PRIMARY KEY (`bundle_id`, `nonbillable_acc_id`),
  INDEX `fk_bundle_has_nonbillable_acc_nonbillable_acc1_idx` (`nonbillable_acc_id` ASC),
  INDEX `fk_bundle_has_nonbillable_acc_bundle1_idx` (`bundle_id` ASC),
  CONSTRAINT `fk_bundle_has_nonbillable_acc_bundle1`
    FOREIGN KEY (`bundle_id`)
    REFERENCES `microsrvbus`.`bundle` (`bundle_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bundle_has_nonbillable_acc_nonbillable_acc1`
    FOREIGN KEY (`nonbillable_acc_id`)
    REFERENCES `microsrvbus`.`nonbillable_acc` (`nonbillable_acc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Not competed yet\n';

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`alacarte_product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`alacarte_product` (
  `alacarte_product_id` INT NOT NULL AUTO_INCREMENT,
  `alacarte_product_name` VARCHAR(45) BINARY NULL,
  `gsm_id` INT NULL,
  `bundle_id` INT NULL,
  `nrc` DECIMAL(10,2) NULL,
  `rc` DECIMAL(10,2) NULL,
  PRIMARY KEY (`alacarte_product_id`),
  INDEX `fk_alacarte_product_GSM1_idx` (`gsm_id` ASC),
  INDEX `fk_alacarte_product_bundle1_idx` (`bundle_id` ASC),
  CONSTRAINT `fk_alacarte_product_GSM1`
    FOREIGN KEY (`gsm_id`)
    REFERENCES `microsrvbus`.`GSM` (`gsm_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_alacarte_product_bundle1`
    FOREIGN KEY (`bundle_id`)
    REFERENCES `microsrvbus`.`bundle` (`bundle_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Not completed yet\n';

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`alacarte_component`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`alacarte_component` (
  `alacarte_component_id` INT NOT NULL AUTO_INCREMENT,
  `alacarte_component_name` VARCHAR(45) NULL,
  `gsm_id` INT NULL,
  `nrc` DECIMAL(10,2) NULL,
  `rc` DECIMAL(10,2) NULL,
  `bundle_id` INT NULL,
  PRIMARY KEY (`alacarte_component_id`),
  INDEX `fk_alacarte_component_GSM1_idx` (`gsm_id` ASC),
  INDEX `fk_alacarte_component_bundle1_idx` (`bundle_id` ASC),
  CONSTRAINT `fk_alacarte_component_GSM1`
    FOREIGN KEY (`gsm_id`)
    REFERENCES `microsrvbus`.`GSM` (`gsm_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_alacarte_component_bundle1`
    FOREIGN KEY (`bundle_id`)
    REFERENCES `microsrvbus`.`bundle` (`bundle_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Not completed yet';

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`purchase_order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`purchase_order` (
  `purchase_order_id` INT NOT NULL AUTO_INCREMENT,
  `purchase_order_desc` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`purchase_order_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`card`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`card` (
  `card_id` INT NOT NULL,
  `iccid1` BIGINT NULL,
  `imsi1` BIGINT NULL,
  `iccid2` BIGINT NULL,
  `imsi2` BIGINT NULL,
  `iccid3` BIGINT NULL,
  `imsi3` BIGINT NULL,
  `iccid4` BIGINT NULL,
  `imsi4` BIGINT NULL,
  `iccid5` BIGINT NULL,
  `imsi5` BIGINT NULL,
  `pin1` VARCHAR(45) NULL,
  `puk1` VARCHAR(45) NULL,
  `pin2` VARCHAR(45) NULL,
  `puk2` VARCHAR(45) NULL,
  `ccpin` VARCHAR(45) NULL,
  `purchase_order_id` INT NOT NULL,
  `notes` TEXT NULL,
  PRIMARY KEY (`card_id`, `purchase_order_id`),
  INDEX `fk_card_purchase_order1_idx` (`purchase_order_id` ASC),
  CONSTRAINT `fk_card_purchase_order1`
    FOREIGN KEY (`purchase_order_id`)
    REFERENCES `microsrvbus`.`purchase_order` (`purchase_order_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`bundle_plan_offered`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`bundle_plan_offered` (
  `bundle_plan_offered_id` INT NOT NULL AUTO_INCREMENT,
  `plan_name` VARCHAR(45) NOT NULL,
  `nrc` DECIMAL(10,2) NOT NULL DEFAULT 0,
  `rc` DECIMAL(10,2) NOT NULL DEFAULT 0,
  `bundle_id` INT NOT NULL,
  PRIMARY KEY (`bundle_plan_offered_id`),
  INDEX `fk_bundle_plan_offered_bundle1_idx` (`bundle_id` ASC),
  CONSTRAINT `fk_bundle_plan_offered_bundle1`
    FOREIGN KEY (`bundle_id`)
    REFERENCES `microsrvbus`.`bundle` (`bundle_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`gsm_plan_slected`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`gsm_plan_slected` (
  `nonbillable_acc_id` INT NOT NULL,
  `gsm_plan_offered_id` INT NOT NULL,
  `gsm_plan_alias` VARCHAR(45) NULL,
  PRIMARY KEY (`nonbillable_acc_id`, `gsm_plan_offered_id`),
  INDEX `fk_gsm_plan_slected_gsm_plan_offered1_idx` (`gsm_plan_offered_id` ASC),
  CONSTRAINT `fk_gsm_plan_slected_nonbillable_acc1`
    FOREIGN KEY (`nonbillable_acc_id`)
    REFERENCES `microsrvbus`.`nonbillable_acc` (`nonbillable_acc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gsm_plan_slected_gsm_plan_offered1`
    FOREIGN KEY (`gsm_plan_offered_id`)
    REFERENCES `microsrvbus`.`gsm_plan_offered` (`gsm_plan_offered_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`bundle_plan_selected`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`bundle_plan_selected` (
  `nonbillable_acc_id` INT NOT NULL,
  `bundle_plan_offered_id` INT NOT NULL,
  `bundle_plan_alias` VARCHAR(45) NULL,
  PRIMARY KEY (`nonbillable_acc_id`, `bundle_plan_offered_id`),
  INDEX `fk_bundle_plan_selected_nonbillable_acc1_idx` (`nonbillable_acc_id` ASC),
  CONSTRAINT `fk_bundle_plan_selected_bundle_plan_offered1`
    FOREIGN KEY (`bundle_plan_offered_id`)
    REFERENCES `microsrvbus`.`bundle_plan_offered` (`bundle_plan_offered_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bundle_plan_selected_nonbillable_acc1`
    FOREIGN KEY (`nonbillable_acc_id`)
    REFERENCES `microsrvbus`.`nonbillable_acc` (`nonbillable_acc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
