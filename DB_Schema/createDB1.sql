SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `microsrvbus` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
SHOW WARNINGS;
USE `microsrvbus` ;

-- -----------------------------------------------------
-- Table `microsrvbus`.`customer_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`customer_category` (
  `customer_category_id` INT NOT NULL AUTO_INCREMENT,
  `customer_category_cd_str` VARCHAR(255) NOT NULL,
  `customer_category_nm_str` VARCHAR(255) NULL,
  PRIMARY KEY (`customer_category_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`reason`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`reason` (
  `reason_id` INT NOT NULL AUTO_INCREMENT,
  `reason_cd_str` VARCHAR(45) NOT NULL,
  `reason_nm_str` VARCHAR(45) NULL,
  PRIMARY KEY (`reason_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`source_of_signup`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`source_of_signup` (
  `source_of_signup_id` INT NOT NULL AUTO_INCREMENT,
  `source_of_signup_cd` VARCHAR(45) NOT NULL,
  `source_of_signup_nm` VARCHAR(45) NULL,
  PRIMARY KEY (`source_of_signup_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`language`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`language` (
  `language_id` INT NOT NULL AUTO_INCREMENT,
  `lang_cd_str` VARCHAR(255) NOT NULL,
  `lang_nm_str` VARCHAR(255) NULL,
  PRIMARY KEY (`language_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`location`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`location` (
  `location_id` INT NOT NULL AUTO_INCREMENT,
  `location_cd_str` VARCHAR(255) NOT NULL,
  `location_nm_str` VARCHAR(255) NULL,
  PRIMARY KEY (`location_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`user` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `user_cd` VARCHAR(45) NOT NULL,
  `invalid_login_count` TINYINT NULL DEFAULT 0,
  `password` VARCHAR(45) NOT NULL,
  `password1` VARCHAR(45) NULL,
  `password2` VARCHAR(45) NULL,
  `password3` VARCHAR(45) NULL,
  `password4` VARCHAR(45) NULL,
  `password5` VARCHAR(45) NULL,
  `password_change_date` DATETIME NULL,
  `password_expiry_date` DATETIME NULL,
  `status` BIT NULL DEFAULT 0,
  `user_name` VARCHAR(45) NOT NULL,
  `department_name` VARCHAR(45) NULL,
  `email_address` VARCHAR(45) NOT NULL,
  `language_id` INT NULL,
  `telephone_no` VARCHAR(45) NULL,
  `user_desc` VARCHAR(255) NULL,
  `multiple_login` BIT NULL,
  `location_id` INT NULL,
  PRIMARY KEY (`user_id`),
  INDEX `fk_user_language1_idx` (`language_id` ASC),
  INDEX `fk_user_location1_idx` (`location_id` ASC),
  CONSTRAINT `fk_user_language1`
    FOREIGN KEY (`language_id`)
    REFERENCES `microsrvbus`.`language` (`language_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_location1`
    FOREIGN KEY (`location_id`)
    REFERENCES `microsrvbus`.`location` (`location_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `microsrvbus`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `microsrvbus`.`customer` (
  `customer_id` INT NOT NULL AUTO_INCREMENT,
  `contact_no` VARCHAR(45) NULL,
  `contact_person` VARCHAR(255) NULL DEFAULT '',
  `contract_no` VARCHAR(45) NULL,
  `salutation_prefix` VARCHAR(255) NULL DEFAULT '',
  `salutation_postfix` VARCHAR(255) NULL,
  `first_name` VARCHAR(255) NULL,
  `middle_name` VARCHAR(255) NULL,
  `last_name` VARCHAR(255) NOT NULL,
  `phone_no` VARCHAR(255) NULL,
  `fax_no` VARCHAR(255) NULL,
  `email_address` VARCHAR(255) NULL,
  `start_date` DATE NULL,
  `stop_date` DATE NULL,
  `contract_stop_date` DATE NULL,
  `customercol` VARCHAR(255) NULL,
  `customercategory_id` INT NOT NULL,
  `customercategory_cd` VARCHAR(45) NULL,
  `no_of_accounts` TINYINT NULL DEFAULT 1,
  `reason_id` INT NOT NULL,
  `reason_cd` VARCHAR(45) NULL,
  `source_of_signup_id` INT NOT NULL,
  `source_of_signup_cd` VARCHAR(45) NULL,
  `sales_agent_id` INT NULL,
  `sales_agent_cd` VARCHAR(45) NULL,
  PRIMARY KEY (`customer_id`),
  INDEX `fk_customer_customercategory1_idx` (`customercategory_id` ASC),
  INDEX `fk_customer_reason1_idx` (`reason_id` ASC),
  INDEX `fk_customer_source_of_signup1_idx` (`source_of_signup_id` ASC),
  INDEX `fk_customer_user1_idx` (`sales_agent_id` ASC),
  CONSTRAINT `fk_customer_customercategory1`
    FOREIGN KEY (`customercategory_id`)
    REFERENCES `microsrvbus`.`customer_category` (`customer_category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_reason1`
    FOREIGN KEY (`reason_id`)
    REFERENCES `microsrvbus`.`reason` (`reason_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_source_of_signup1`
    FOREIGN KEY (`source_of_signup_id`)
    REFERENCES `microsrvbus`.`source_of_signup` (`source_of_signup_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_user1`
    FOREIGN KEY (`sales_agent_id`)
    REFERENCES `microsrvbus`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
